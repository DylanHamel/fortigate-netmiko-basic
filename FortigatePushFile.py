#!/usr/bin/env python

'''
Netmiko commands informations
Netmiko commonly-used methods:

net_connect.send_command() - Send command down the channel, return output back (pattern based)
net_connect.send_command_timing() - Send command down the channel, return output back (timing based)
net_connect.send_config_set() - Send configuration commands to remote device
net_connect.send_config_from_file() - Send configuration commands loaded from a file
net_connect.save_config() - Save the running-config to the startup-config
net_connect.enable() - Enter enable mode
net_connect.find_prompt() - Return the current router prompt
net_connect.commit() - Execute a commit action on Juniper and IOS-XR
net_connect.disconnect() - Close the connection
net_connect.write_channel() - Low-level write of the channel
net_connect.read_channel() - Low-level write of the channel
'''

import netmiko
import sys
import getopt

def main (argv):
    
    # Set variables value
    exit_code = 0
    ipAddress, filename = getArguments(argv)
    fortigate = {
        'device_type': 'fortinet',
        'username': 'admin',
        'password': '',
        'host': ipAddress
    }
    
    print("[FortigateConnecter - main] arguments - |", ipAddress, "|", filename, "|")

    try:
        connector = netmiko.ConnectHandler(**fortigate)
        connector.send_config_from_file(filename)
    except Exception as error:
        exit_code = 2
        print(error)

    print("--- END ---")
    exit(exit_code)

# --------------------------------------------------------------------------------------------------
#
#
#
def getArguments (argv) :
    try :
        opts, args = getopt.getopt(sys.argv[1:], 'hi:p:', ['--help', 'ip=', "path="])

    except getopt.GetoptError as err:
        print(str(err))
        sys.exit(2)

    # Set the default value
    ipAddress = "192.168.1.99"
    path = "./fsdfsdfsdfsdf.txt"

    # Set value with values
    for o, a in opts :
        if o in ("-h" or "--help") :
            print("Run the scritp \n # ./FortigateConnecter.py \n you can give a parameter : \n -i FortigateIPAddresse \n if ipAddress is not specify the value will be set at 192.168.1.99")
        elif o in ("-i" or "--ip"):
            ipAddress = a
        elif o in ("-p" or "--path"):
            path = a
    
    return ipAddress, path

# --------------------------------------------------------------------------------------------------
#
#
#
if __name__ == "__main__":
   main(sys.argv[1:])