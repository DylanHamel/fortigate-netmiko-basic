# FortigatePushOutputConf with Netmiko

This script uses ```netmiko``` to connect to a Fortigate device.
it will copy a file on the Fortigate.

It's a very very easy altenative to ```fortigate-upload-configfile```

run the script
```bash
chmod 550 FortigateConnecter.py # or more permission 777 | 770
./FortigateConnecter.py -i 192.168.255.1 -p /root/configfile

# [-i] = Fortigate ip address
# [-p] = File configuration file that you want push on Fortigate
```

Ansible utilisation is not very adapted.
The loop will be used only on one host.
The script will not be executed on several devices on on the same time

Make a loop with a list in parameters is similar
